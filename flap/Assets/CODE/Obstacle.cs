﻿using UnityEngine;
using System.Collections;

public class Obstacle : MonoBehaviour {

	float miny = 9;
	float maxy = 13;

	void Start () {
		Move ();
	}

	void Update () {
	
	}

	public void Move()
	{
		iTween.MoveTo(gameObject,iTween.Hash("y",Random.Range(miny,maxy), "easeType", "easeInOutExpo", "delay", .1));
	}
}
