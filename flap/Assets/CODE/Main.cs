﻿using UnityEngine;
using System.Collections;

public class Main : MonoBehaviour {
	public static Main instance = null;

	public bool lost = false;
	public int score = 0;

	void Start () {
		Physics.gravity = Vector3.down*12;
		instance = this;
	}
	

	void Update () {
	
	}

	public void OnGUI()
	{
		//TODO show score
		GUIStyle style = new GUIStyle();
		style.alignment = TextAnchor.MiddleCenter;
		style.fontSize = 50;

		GUI.Box(new Rect(0,0,100,100),score.ToString(),style);

		if(lost)
		{
			GUI.Box(new Rect(0,0,Screen.width,Screen.height),"YOU LOSE",style);
		}

	}
}
