﻿using UnityEngine;
using System.Collections;

public class Bird : MonoBehaviour {

	public bool primary = false;
	KeyCode moveKey = KeyCode.None;
	void Start () {
		if(primary) moveKey = KeyCode.A;
		else moveKey = KeyCode.L;

		if(primary) rigidbody.velocity = Vector3.left*2f;
		else rigidbody.velocity = -Vector3.left*2f;
	}
	
	// Update is called once per frame
	void Update () {
		Movement();
	}

	void Movement()
	{
		if(Input.GetKeyDown(moveKey))
		{
			Vector3 oldVel = rigidbody.velocity;
			oldVel.y = 8;
			rigidbody.velocity = oldVel;
		}
	}


	void OnTriggerEnter(Collider col)
	{
		if(col.name.Contains("wall"))
		{
			Vector3 oldVel = rigidbody.velocity;
			oldVel.x = - oldVel.x;
			rigidbody.velocity = oldVel;
			Vector3 oldTrans = transform.eulerAngles;
			oldTrans.y += 180;
			transform.eulerAngles = oldTrans;
		}
	}
	void OnTriggerExit(Collider col)
	{
		if(primary)
			if(col.name == "PASS")
				Main.instance.score += 1;

	}

	void OnCollisionEnter(Collision col)
	{
		if(col.gameObject.name == "BIRD")
		{
			if(primary)
			{
				//Lose ();
			}
		}
		else
			Lose ();
	}

	void Lose()
	{
		if(!Main.instance.lost)
		{
			Main.instance.lost = true;
			StartCoroutine(
				"Restart"
			);
		}
	}

	IEnumerator Restart()
	{
		yield return new WaitForSeconds(3);
		Application.LoadLevel(Application.loadedLevel);
	}

}
